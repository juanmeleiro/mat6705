\documentclass{article}

\usepackage{personal}
\usepackage{bussproofs}

% \setmainfont{}
% \setmonofont{}
% \setsansfont{}
\setmainlanguage{brazil}

\title{
	Tipos, Teoria da Prova e Semântica\\
	Exemplos e Aplicações
}

\author{}
\date{}

\newcommand{\tuple}[1]{\left\langle#1\right\rangle}

\newcommand{\Type}{\texttt{Type}}
\newcommand{\WFT}{~\texttt{wft}}

\begin{document}
    \maketitle

	\section{Introdução}
	
	Sistemas de Tipos são objetos formais que capturam certos aspectos 
	estruturais de lógicas ou de linguagens de programação. Na matemática,
	por exemplo na Álgebra Universal, é praxe estabelecer uma assinatura 
	de tipos, símbolos funcionais e constantes e considerar estruturas sobre
	esta assinatura. Enquanto a Álgebra Universal se ocupa de teoremas sobre 
	igualdade ou relações entre termos sobre a assinatura, a Teoria dos Tipos
	se ocupa de teoremas sobre a relação de habitação entre termos e tipos de
	um determinado sistema de tipos.

	Também é comum considerar tipos dependentes e polimorfismos paramétricos: 
	tipos dependentes nos permitem escrever um Tipo parametrizado por termos 
	de outros Tipos fixos, são \emph{Tipos em contexto}; enquanto polimorfismo 
	paramétrico nos permite tratar de Tipos que dependem de outros \emph{Tipos} 
	variáveis.

	Todos estes Sistemas de Tipos tem uma interpretação Prova-Teórica, com 
	tipos simples correspondendo a Proposições lógicas; Tipos Dependentes 
	correspondendo a Predicados e Tipos Polimórficos correspondendo a 
	predicados de ordem superior (haja visto que há uma sorte de dependência 
	sobre Tipos, que são como que fórmulas lógicas).

	A relação disso com a teoria da prova é relativamente direta: se e só se 
	for possível deduzir dentro da Teoria dos Tipos que um termo habita um tipo 
	com em um contexto vazio, a proposição ao qual o tipo corresponde será
	demonstrável na lógica intuicionista. Semelhantemente, sse houver uma prova
	tipo-teórica que o tipo $\prod_{x:X} P(x)$ é habitado em contexto vazio, a 
	sentença $(\forall x: X) P(x)$ será demonstrável intuicionisticamente. 
	Analogamente $\sum_{x:X} P(x)$ corresponde a $(\exists x: X) P(x)$ e 
	semelhantemente para tipos polimórficos.

	A interpretação categorial de um sistema de tipos pode ser vista como uma 
	semântica funtorial atingindo uma categoria rica o suficiente para 
	expressar os conteúdos tipo-teóricos do sistema. Naturalmente, podemos 
	interpretar sistemas bem comportados o suficiente em um Topos qualquer -- 
	ademais, há resultados de classificação de modelos de sistemas de tipos.

	Nossa abordagem vai se basear nos trabalhos de \note{citar FLORIAN RABE}
	pois vamos tratar de semântica e teoria da prova, coisa que foi feita de 
	maneira categorial e uniforme no trabalho citado. O cerne deste é 
	formalizar os alicerces lógicos ou metateóricos com o objetivo de fazê-los
	efetivos na implementação de verificadores de teorema e assistentes de 
	provas de maneira formalista.

	\section{Prelúdio}

	\begin{definition}[Categoria com Inclusões]
		Uma \emph{categoria com inclusões} é uma categoria munida de uma 
		subcategoria que seja uma ordem parcial nos objetos da mesma.
		Escrevemos $A\hookrightarrow B$ para o morfismo da categoria que 
		testemunha a desigualdade $A\leq B$.

		Neste contexto, chamamos os objetos de uma categoria com inclusões
		de \emph{teorias} e as flechas entre elas de \emph{morfismos de 
		teorias}; na ocasião que considerarmos um morfismo de teoria que 
		seja $A\hookrightarrow B$, diremos que o morfismo em questão é a 
		\emph{inclusão} de $A$ em $B$, ou que $B$ extende $A$ por via da 
		inclusão.
	\end{definition}

	\begin{definition}[Teorias e Expressões]
		Uma teoria, neste contexto, é -- intutivamente -- uma sequência de 
		declarações de identificadores para qual não há dependências cíclicas
		nas definições. Formalmente, isto diz que um identificador só pode 
		depender das expressões formáveis pelas declarações que o precedem. 
		Além destas exigências, pedimos também para que não haja duas 
		declarações do mesmo identificador.

		As expressões sobre uma teoria são como que um subconjunto do monoide 
		livremente gerado por um conjunto de identificadores. A definição 
		precisa segue.

		Fixado um conjunto de identificadores $I$, uma teoria $\Sigma$ tem a 
		seguinte forma:
		\begin{enumerate}
			\item A teoria vazia: “$\emptyset$”;
			\item Uma teoria $\Gamma$ seguida por uma declaração de um 
			identificador que não ocorre em $\Gamma$ com uma 
			$\Gamma$-expressão: “$\Gamma,(c : E)$”;
			\item \note{Algo que diga sobre teorias infinitas}.
		\end{enumerate}
		
		Quando possível, escrevemos “$\Sigma,\Gamma$” para a concatenação de 
		duas teorias $\Sigma$ e $\Gamma$. Nem sempre isto será uma teoria, por
		força de repetição de identificadores. Pode ser, também, que o reverso
		ocorra: que “$\Sigma,\Gamma$” seja uma teoria sem que $\Gamma$ o seja.
		Isto é claro pois as declarações de $\Gamma$ podem depender em 
		declarações que ocorreram em $\Sigma$.

		Uma $\Sigma$-expressão tem a segunte forma:
		\begin{enumerate}
			\item Um identificador $c$ que fora declarado em $\Sigma$;
			\item A sequência “$C(\Gamma; c_1, \cdots, c_n)$” onde 
			$C$ é um identificador qualquer; $c_i$ são todos
			$(\Sigma,\Gamma)$-expressões (e em particular ($\Sigma,\Gamma$) é 
			uma teoria de fato).
		\end{enumerate}

		Dizemos que algo é uma \emph{expressão} sem qualificações da teoria 
		sobre a qual ela é expressão se este algo for uma expressão sobre 
		alguma teoria.
	\end{definition}

	\begin{definition}[Teorias 2.0]
		Teorias podem ser redefinidas como uma boa-ordem 
		$\tuple{\dom(\Sigma), <}$ munida de uma operação $\Sigma$ que mapeia um
		identificador $c\in\dom(\Sigma)$ para uma 
		$(\Sigma\restriction\set{c'\in\dom(\Sigma)}{c'<c})$-expressão, que é 
		entendida como o tipo de $c$ como seria declarado na definição anterior 
		de teorias.

		Apesar de pouco prática para se definir uma teoria, esta definição 
		facilita a escrita de outras partes do formalismo, então introduziremos
		a notação $\Sigma(c)$ para a expressão $E$ para a qual $c:E$ ocorra em 
		$\Sigma$.
	\end{definition}

	Nesta estrutura de teorias não há uma tipagem explícita. Pela natureza do 
	programa de Rabe, as teorias são construídas como torres cada vez mais 
	específicas, então introduzimos dois tipos de julgamentos sobre uma teoria
	$\Sigma$ que escrevemos:
	\[
		\vdash_\Sigma t : T
	\]
	\[
		\vdash_\Sigma T \WFT
	\]

	Que lemos como “$t$ habita o tipo $T$” e “$T$ pode aparecer como o tipo de 
	algum $t$”. É típico também que escrevamos, como substituto de 
	$\vdash_\Sigma t : T$, o seguinte:
	\[
		\vdash_\Sigma T
	\]
	Que pode ser lido como “O tipo $T$ é habitado”, sem -- no entanto --
	especificar quem o habita.

	\begin{definition}[Regras]
		As regras neste contexto do programa de Rabe são extremamente genéricas
		-- na sua formulação mais geral temos: uma regra $n$-ária é uma forma 
		de decidir se julgamentos em teorias diversas são admissiveis de serem
		suas premissas e uma forma de transformar estes julgamentos em um outro
		julgamento chamado conclusão.

		Na prática (e -- de fato -- em \note{citar o Rabe novamente}) não se 
		define desta maneira, o que costuma-se fazer é o seguinte:
		uma regra para uma teoria $\Sigma$ é da seguinte forma:
		\newcommand{\Meta}{\texttt{Meta}}
		\begin{prooftree}
			\AxiomC{$\vdash_{(\Sigma,\Meta,\Gamma_1)} t_1 : T_1$}
			\AxiomC{$\cdots$}
			\AxiomC{$\vdash_{(\Sigma,\Meta,\Gamma_k)} t_k : T_k$}
			\TrinaryInfC{
					$\vdash_{(\Sigma,\Meta)} t_0 : T_0$
			}
		\end{prooftree}
		Onde $\Meta$ é a teoria das meta-variáveis para a regra e os $\Gamma_i$ 
		são entendidos como declarações locais de variáveis. 
		
		A interpretação desta definição é quando instanciamos uma regra estamos
		escolhendo -- para cada declaração de meta-variáveis -- uma expressão 
		em $\Sigma$; que induz um mapa das $(\Sigma,\Meta,\Gamma_i)$-expressões
		para expressões adequadas que podemos usar para verificar se as 
		premissas estão satisfeitas e então aplicar a regra e obter a 
		conclusão.

		É \emph{praxe} ignorar problemas de conflito de nomes para os 
		identificadores de $\Meta$ e distingui-los dos de $\Sigma$. 
	\end{definition}

	\begin{example}[Regras]
		\newcommand{\derives}{\supset}
		\newcommand{\prop}{\texttt{prop}}
		\newcommand{\Meta}{\texttt{Meta}}
		Um fragmento da lógica proposicional é:
		\[
			\Sigma = (
				\top: \prop,
				\bot: \prop,
				\derives:	\prop \to 
							\prop \to
							\prop,
				\land:		\prop \to 
							\prop \to
							\prop
			)
		\]

		Podemos escrever a regra de eliminação 
		da implicação como:
		\[
			\Meta = (A : \prop, B : \prop)
		\]
		\begin{prooftree}
			\AxiomC{$\Sigma,\Meta\vdash A$}
			\AxiomC{$\Sigma,\Meta \vdash A \derives B$}
			\BinaryInfC{$\Sigma,\Meta \vdash B$}
		\end{prooftree}
	\end{example}

\end{document}
